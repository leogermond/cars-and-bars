with Mage;
with Mage.Model;
with Mage.Event;
with Mage.Draw;
with Mage.Input;

procedure Cars_And_Bars is
   W             : Mage.Draw.Window_ID;
   C             : Mage.Draw.Canvas_ID;
   Screen_Width_Px : constant Positive := 800;
   Screen_Height_Px : constant Positive := 200;

   ZF : constant Float := 2.0;
   Screen_Width : constant Mage.Float_Pos := Float (Screen_Width_Px) / ZF;
   Screen_Height : constant Mage.Float_Pos := Float (Screen_Height_Px) / ZF;

   --  all dimensions are in percent of the screen height
   type Screen_Percent is new Float range -1.0 .. 1.0;
   subtype Screen_Percent_Pos is Screen_Percent range 0.0 .. 1.0;

   function To_Screen_W (V : Screen_Percent) return Float is
     (Float (V) * Screen_Width);
   Screen_W_Percent_Resolution : constant Screen_Percent_Pos :=
     Screen_Percent (1.0 / Screen_Width);

   function To_Screen_H (V : Screen_Percent) return Float is
     (Float (V) * Screen_Height);
   Screen_H_Percent_Resolution : constant Screen_Percent_Pos :=
     Screen_Percent (1.0 / Screen_Height);

   type Screen_Percent_Centered is new Screen_Percent range -0.5 .. 0.5;

   function Centered (V : Screen_Percent_Pos) return Screen_Percent_Centered is
     (Screen_Percent_Centered (V - 0.5));

   function Absolute (V : Screen_Percent_Centered) return Screen_Percent is
     (Screen_Percent (V + 0.5));

   function To_3d
     (X, Y : Screen_Percent_Centered) return Mage.Model.Point_3d is
     ((To_Screen_W (X), To_Screen_H (Y), 0.0));

   procedure Rect
     (X, Y, W, H : Screen_Percent_Pos; Color : Mage.RGBA_T;
      Fill       : Boolean := False) with
     Pre =>
      W >= Screen_W_Percent_Resolution
      and then H >= Screen_H_Percent_Resolution
   is
      W_Scr : constant Float := To_Screen_W (W);
      H_Scr : constant Float := To_Screen_H (H);
      pragma Assert (W_Scr >= 1.0, W_Scr'Image);
      pragma Assert (H_Scr >= 1.0, H_Scr'Image);
   begin
      if Fill then
         Mage.Draw.Draw_Fill_Rect
           (C, To_3d (Centered (X), Centered (Y)), W_Scr, H_Scr, Color);
      else
         Mage.Draw.Draw_Rect
           (C, To_3d (Centered (X), Centered (Y)), W_Scr, H_Scr, Color);
      end if;
   end Rect;

   Car_Width  : constant Screen_Percent_Pos := 0.08;
   Car_Height : constant Screen_Percent_Pos := 0.12;

   procedure Draw_Road
     (X_Offset : Screen_Percent_Pos; Height : Screen_Percent_Pos)
   is
      Sidewalk_Height_Ratio  : constant := 0.1;
      Grass_Height_Ratio     : constant := 1.0 - Sidewalk_Height_Ratio;
      Road_Mark_Height_Ratio : constant := 0.12;

      --  ratios over the whole screen, so... absolute screen percentage
      --  both together must be so that 1 /(W + S) ~= N, with N an integer
      --  otherwise the road looping will be weird
      Road_Mark_Width         : constant := 0.08;
      Road_Mark_Width_Spacing : constant := 0.12;

      Sidewalk_Height : constant Screen_Percent_Pos :=
        Sidewalk_Height_Ratio * (1.0 - Height) / 2.0;
      Grass_Height    : constant Screen_Percent_Pos :=
        Grass_Height_Ratio * (1.0 - Height) / 2.0;
   begin
      Rect (0.0, 0.0, 1.0, Grass_Height, (60, 180, 0, 0), Fill => True);
      Rect
        (0.0, Grass_Height, 1.0, Sidewalk_Height, (180, 180, 180, 0),
         Fill => True);

      Rect
        (0.0, Grass_Height + Sidewalk_Height, 1.0, Height, (50, 50, 40, 0),
         Fill => True);

      --  roadmarks
      for J in 0 .. Integer (1.0 / (Road_Mark_Width + Road_Mark_Width_Spacing))
      loop
         declare
            Start_X : constant Float :=
              Float (J) * (Road_Mark_Width + Road_Mark_Width_Spacing) +
              Float (X_Offset);

            procedure Road_Mark
              (X : Screen_Percent_Pos; Road_Mark_Width : Screen_Percent_Pos)
            is
            --  Draw a road mark, or not if size is too small
            begin
               if Road_Mark_Width >= Screen_W_Percent_Resolution then
                  Rect
                    (X,
                     Grass_Height + Sidewalk_Height +
                     Height * (1.0 - Road_Mark_Height_Ratio) / 2.0,
                     Road_Mark_Width, Road_Mark_Height_Ratio * Height,
                     (210, 210, 200, 0), Fill => True);
               end if;
            end Road_Mark;
         begin
            if Start_X <=
              Float (Screen_Percent_Pos'Last) - Float (Road_Mark_Width)
            then
               --  completely drawable
               Road_Mark (Screen_Percent_Pos (Start_X), Road_Mark_Width);
            elsif Start_X >= Float (Screen_Percent_Pos'Last) then
               --  out-of-screen: loop back to starting column
               Road_Mark
                 (Screen_Percent_Pos
                    (Start_X - Float (Screen_Percent_Pos'Last)),
                  Road_Mark_Width);
            else
               --  split in two parts between first and last column
               declare
                  Width_Remainder : constant Screen_Percent_Pos :=
                    Screen_Percent_Pos
                      (Float (Screen_Percent_Pos'Last) - Start_X);
                  Width_Overflow  : constant Screen_Percent_Pos :=
                    Road_Mark_Width - Width_Remainder;
               begin
                  Road_Mark (Screen_Percent_Pos (Start_X), Width_Remainder);
                  Road_Mark (0.0, Width_Overflow);
               end;
            end if;
         end;
      end loop;

      Rect
        (0.0, 1.0 - (Grass_Height + Sidewalk_Height), 1.0, Sidewalk_Height,
         (180, 180, 180, 0), Fill => True);
      Rect
        (0.0, 1.0 - Grass_Height, 1.0, Grass_Height, (60, 180, 0, 0),
         Fill => True);
   end Draw_Road;

   procedure Draw_Car (X : Screen_Percent_Pos; Y_C : Screen_Percent_Centered)
   is
      Left_To_Right           : constant Boolean := True;
      Front_Hood_Width_Ratio  : constant         := 1.2 / 3.0;
      Front_Hood_Height_Ratio : constant         := 8.0 / 10.0;
      Back_Trunk_Width_Ratio  : constant         := 0.8 / 3.0;
      Back_Trunk_Height_Ratio : constant         := 6.0 / 10.0;
      Windshield_Width_Ratio  : constant         := 0.5 / 3.0;
      Windshield_Height_Ratio : constant         := 8.0 / 10.0;

      Body_Color              : constant Mage.RGBA_T := (255, 40, 10, 0);
      Hood_Color, Trunk_Color : constant Mage.RGBA_T := (200, 80, 80, 0);
      Windshield_Color        : constant Mage.RGBA_T := (50, 50, 50, 0);

      Y : constant Screen_Percent_Pos := Absolute (Y_C);

      --  all on the right
      Front_Hood_Width_Offset_Ratio  : constant :=
        (1.0 - Front_Hood_Width_Ratio);
      --  right before the hood
      Windshield_Width_Offset_Ratio  : constant :=
        Front_Hood_Width_Offset_Ratio - Windshield_Width_Ratio;
      --  split even top / down
      Front_Hood_Height_Offset_Ratio : constant :=
        (1.0 - Front_Hood_Height_Ratio) / 2.0;
      Back_Trunk_Height_Offset_Ratio : constant :=
        (1.0 - Back_Trunk_Height_Ratio) / 2.0;
      Windshield_Height_Offset_Ratio : constant :=
        (1.0 - Windshield_Height_Ratio) / 2.0;
   begin
      --  Body
      Rect (X, Y, Car_Width, Car_Height, Body_Color, Fill => True);
      --  Hood
      Rect
        (X + Front_Hood_Width_Offset_Ratio * Car_Width,
         Y + Front_Hood_Height_Offset_Ratio * Car_Height,
         Front_Hood_Width_Ratio * Car_Width,
         Front_Hood_Height_Ratio * Car_Height, Hood_Color, Fill => True);
      --  Trunk
      Rect
        (X, Y + Back_Trunk_Height_Offset_Ratio * Car_Height,
         Back_Trunk_Width_Ratio * Car_Width,
         Back_Trunk_Height_Ratio * Car_Height, Trunk_Color, Fill => True);
      --  Windshield
      Rect
        (X + Windshield_Width_Offset_Ratio * Car_Width,
         Y + Windshield_Height_Offset_Ratio * Car_Height,
         Windshield_Width_Ratio * Car_Width,
         Windshield_Height_Ratio * Car_Height, Windshield_Color, Fill => True);
   end Draw_Car;

   subtype Y_Car_Screen_Percent_Centered is
     Screen_Percent_Centered range Screen_Percent_Centered'First ..
         Screen_Percent_Centered'Last - Screen_Percent_Centered (Car_Height);

   X_Car_Min_Speed   : constant Screen_Percent_Pos            := 0.01;
   X_Car_Max_Speed   : constant Screen_Percent_Pos            := 0.04;
   X_Car_Acc_Ratio   : constant                               := 1.01;
   X_Car_Deacc_Ratio : constant                               := 0.98;
   X_Car_Speed       : Screen_Percent_Pos := X_Car_Min_Speed;
   Y_Car_Speed       : constant Y_Car_Screen_Percent_Centered := 0.01;
   X_Car             : Screen_Percent_Pos                     := 0.0;
   Y_Car             : Y_Car_Screen_Percent_Centered          := 0.0;
begin
   W := Mage.Draw.Create_Window (Screen_Width_Px, Screen_Height_Px, "Cars & Bars");
   C := Mage.Draw.Get_Canvas (W);
   Mage.Draw.Zoom_Factor (C, ZF);

   while not Mage.Event.Is_Killed loop
      declare
         use Mage.Input;
         Actions : constant Action_Set := Global_Actions;
         Accel   : Boolean             := True;
      begin

         if Has_Action (Actions, Up) then
            if Y_Car >= Y_Car_Screen_Percent_Centered'First + Y_Car_Speed then
               Y_Car := Y_Car - Y_Car_Speed;
            end if;
            Accel := False;
         end if;

         if Has_Action (Actions, Down) then
            if Y_Car <= Y_Car_Screen_Percent_Centered'Last - Y_Car_Speed then
               Y_Car := Y_Car + Y_Car_Speed;
            end if;
            Accel := False;
         end if;

         if Accel then
            if X_Car_Speed < X_Car_Max_Speed then
               X_Car_Speed := X_Car_Speed * X_Car_Acc_Ratio;
            end if;
         elsif X_Car_Speed > X_Car_Min_Speed then
            X_Car_Speed := X_Car_Speed * X_Car_Deacc_Ratio;
         end if;
      end;

      if X_Car < Screen_Percent_Pos'First + X_Car_Speed then
         X_Car := Screen_Percent_Pos'Last;
      else
         X_Car := X_Car - X_Car_Speed;
      end if;

      Draw_Road (X_Car, 0.6);
      Draw_Car (0.1, Y_Car);
      Mage.Event.Handle_Events (W);
      delay 1.0 / 60.0;
   end loop;
end Cars_And_Bars;
